//
//  CollectionViewBaseCell.swift
//  SwippingMenuCollectionComponent
//
//  Created by Haiyan Ma on 06/04/2017.
//  Copyright © 2017 SwiftTsubame. All rights reserved.
//

import UIKit

class BaseCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
    }
}
