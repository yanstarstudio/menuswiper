//
//  CellDatasource.swift
//  SwippingMenuCollectionComponent
//
//  Created by Haiyan Ma on 06/04/2017.
//  Copyright © 2017 SwiftTsubame. All rights reserved.
//

import UIKit

struct CellDataSource {
    var title: String?
    var imageName: String?
    var alignment: UILayoutConstraintAxis?
    
    func imageHidden() -> Bool {
        if imageName == nil {
            return true
        } else {
            return false
        }
    }
    
    func titleHidden() -> Bool {
        if title == nil {
            return true
        } else {
            return false
        }
    }
}
