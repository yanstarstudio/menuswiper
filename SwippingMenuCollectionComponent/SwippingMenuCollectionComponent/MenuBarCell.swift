
//
//  MenuBarCe.swift
//  SwippingMenuCollectionComponent
//
//  Created by Haiyan Ma on 05/04/2017.
//  Copyright © 2017 SwiftTsubame. All rights reserved.
//

import UIKit

class MenuBarCell: BaseCell {
    
    let label: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    override var isHighlighted: Bool {
        didSet {
            label.textColor = isHighlighted ? textColorOnSelection : textColorDefault
            imageView.tintColor = isSelected ? textColorOnSelection : textColorDefault
        }
    }
    
    override var isSelected: Bool {
        didSet {
            label.textColor = isSelected ? textColorOnSelection : textColorDefault
            imageView.tintColor = isSelected ? textColorOnSelection : textColorDefault
        }
    }
    
    var cellDatasource: CellDataSource? {
        didSet {
            guard let cellDatasource = cellDatasource else { return }
            label.text = cellDatasource.title
            if let imageName = cellDatasource.imageName {
                imageView.image = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
                imageView.tintColor = isSelected ? textColorOnSelection : textColorDefault
            }
            if let alignment = cellDatasource.alignment {
                setupStackview(cellDatasource: cellDatasource, alignment: alignment)
            } else {
                setupStackview(cellDatasource: cellDatasource)
            }
        }
    }
    
    var textColorOnSelection: UIColor = MenuBarStyle.onSelectionTitleColor
    var textColorDefault: UIColor = MenuBarStyle.defaultTitleColor
    var textFont: UIFont = MenuBarStyle.titleFont
    
    func titleStyle(colorOnSelection: UIColor?, colorDefault: UIColor?, font: UIFont?) {
        if let colorOnSelection = colorOnSelection {
            textColorOnSelection = colorOnSelection
        }
        
        if let colorDefault = colorDefault {
            textColorDefault = colorDefault
        }
        
        if let font = font {
            textFont = font
        }
        
        imageView.tintColor = isSelected ? textColorOnSelection : textColorDefault
        label.textColor = isSelected ? textColorOnSelection : textColorDefault
        label.font = font
    }
    
    override func setupViews() {
        super.setupViews()
        setupStackview(cellDatasource: cellDatasource)
    }

    func setupStackview(cellDatasource: CellDataSource?, alignment: UILayoutConstraintAxis = UILayoutConstraintAxis.horizontal) {

        guard let cellDatasource = cellDatasource else { return }
        
        // Create 2 views to contain image and text
        let imageContainerView = UIView()
        imageContainerView.translatesAutoresizingMaskIntoConstraints = false
        let textContainerView = UIView()
        textContainerView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(imageContainerView)
        addSubview(textContainerView)
        
        // Set if views are hidden or visible
        imageContainerView.isHidden = cellDatasource.imageHidden()
        textContainerView.isHidden = cellDatasource.titleHidden()
        
        // Arrange Stackview and fine tune image-Text proportion
        let stackView = UIStackView(arrangedSubviews: [imageContainerView, textContainerView])
        
        if !cellDatasource.imageHidden() && !cellDatasource.titleHidden() {
            if alignment == .horizontal {
                imageContainerView.widthAnchor.constraint(equalTo: textContainerView.widthAnchor, multiplier: 0.4).isActive = true
            } else {
                imageContainerView.widthAnchor.constraint(equalTo: textContainerView.widthAnchor, multiplier: 1) .isActive = true
                imageContainerView.heightAnchor.constraint(equalTo: textContainerView.heightAnchor, multiplier: 1.6).isActive = true
            }
        }

        stackView.axis = alignment
        stackView.distribution = .fillProportionally
        stackView.spacing = 10
        addSubview(stackView)
        
        // Align stackview to the cell
        stackView.anchorWithConstantsToTop(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 5, leftConstant: 5, bottomConstant: 5, rightConstant: 5)
        stackView.topAnchor.constraint(greaterThanOrEqualTo: topAnchor, constant: 5).isActive = true

        // Align label and image wrt their container
        textContainerView.addSubview(label)
        imageContainerView.addSubview(imageView)
        label.fillSuperview()
        imageView.fillSuperview()
        
        // Alignment Label text
        alignmentOfLabel(imageHiden: cellDatasource.imageHidden(), alignment: alignment)
    }
    
    func alignmentOfLabel(imageHiden: Bool, alignment: UILayoutConstraintAxis) {
        if imageHiden || alignment == .vertical {
            label.textAlignment = .center
        } else {
            label.textAlignment = .left
        }        
    }
}


