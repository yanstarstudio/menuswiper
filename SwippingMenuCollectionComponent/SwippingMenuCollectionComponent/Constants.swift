//
//  Constants.swift
//  SwippingMenuCollectionComponent
//
//  Created by Haiyan Ma on 06/04/2017.
//  Copyright © 2017 SwiftTsubame. All rights reserved.
//

import UIKit

struct MenuBarStyle {
    
    static let menuBackgroundColor = UIColor.white
    
    static let defaultTitleColor = UIColor.black
    static let onSelectionTitleColor = UIColor.red
    static let titleFont = UIFont.boldSystemFont(ofSize: 18)
    static let bottomLineColor = UIColor.rgb(205, green: 206, blue: 206)
    enum Axis {
        case Horiental, Vertical
    }
    static let defaultCellHeight: CGFloat = 80.0
}
