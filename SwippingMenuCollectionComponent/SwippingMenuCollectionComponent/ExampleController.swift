//
//  TestController.swift
//  SwippingMenuCollectionComponent
//
//  Created by Haiyan Ma on 05/04/2017.
//  Copyright © 2017 SwiftTsubame. All rights reserved.
//

import UIKit

class TestController: TopMenuCollectionViewController {
    
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuDatasource = (text: ["Menu 1", "Menu 2", "Menu 3"],
                          image: ["trending", "trending", "trending"])
        
        menuBarHeight = 80
        titleStyle(colorOnSelection: .blue, colorDefault: .gray, font: UIFont.boldSystemFont(ofSize: 20))
        menuStyle(backgroundColor: .yellow, sliderColor: .magenta)
        menuAlignment = .vertical
        
        collectionView?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        return cell
    }
    

}
