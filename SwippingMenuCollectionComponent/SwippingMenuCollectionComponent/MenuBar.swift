//
//  MenuBar.swift
//  SwippingMenuCollectionComponent
//
//  Created by Haiyan Ma on 05/04/2017.
//  Copyright © 2017 SwiftTsubame. All rights reserved.
//

import UIKit

class MenuBar: UIView {
    
    var horizontalBarLeftAnchorConstraint: NSLayoutConstraint?
    var horizontalBarWidthANchorConstraint: NSLayoutConstraint?
    
    // Title style
    var menuTextColor: UIColor = MenuBarStyle.defaultTitleColor
    var menuTextSelectedColor: UIColor = MenuBarStyle.onSelectionTitleColor
    var titleFont = MenuBarStyle.titleFont

    // Background
    var barBackgroundColor: UIColor = MenuBarStyle.menuBackgroundColor
    
    // Slider
    var sliderBackgroundColor: UIColor = MenuBarStyle.onSelectionTitleColor {
        didSet {
            swipingBarView.backgroundColor = sliderBackgroundColor
        }
    }
    
    // Bottom Line
    var bottomDividerLineColor: UIColor = MenuBarStyle.bottomLineColor {
        didSet {
            bottomSeparator.backgroundColor = bottomDividerLineColor
        }
    }
    
    let cellId = "cellId"
    
    // Collectionview to carry menu items inside
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    let swipingBarView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let bottomSeparator: UIView = {
        return UIView()
    }()

    
    var menuBarLabelTextArray: [String]?
    
    var menuBarImageArray: [String]?
    
    var menuAlignment: UILayoutConstraintAxis? {
        didSet {
            updateCollectionView()
        }
    }
    
    var numberOfItem: Int? {
        didSet {
            guard let number = numberOfItem else { return }
            setupHorizontalSliderWidthConstraint(with: number)
            updateHorizontalBarConstraint(with: number)
            updateCollectionView()
        }
    }
    
    weak var delegate: MenuScrollDelegate?
    
    // MARK: Menu Bar Setup
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCollectionView()
        setupHorizontalSliderBar()
        setupBottomDividerLine()
    }
    
    fileprivate func setupCollectionView() {
        addSubview(collectionView)
        collectionView.fillSuperview()
        collectionView.register(MenuBarCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.allowsSelection = true
        // Preselect item 0, section 0 collectionview cell
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())
//        collectionView.reloadData()
    }
    
    // layout Bottom sliding Bar
    fileprivate func setupHorizontalSliderBar() {
        
        addSubview(swipingBarView)
        swipingBarView.backgroundColor = sliderBackgroundColor
        
        // LeftAnchor
        horizontalBarLeftAnchorConstraint = swipingBarView.leftAnchor.constraint(equalTo: self.leftAnchor)
        horizontalBarLeftAnchorConstraint?.isActive = true
        
        // Bottom Anchor
        swipingBarView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
                
        // Height Anchor
        swipingBarView.heightAnchor.constraint(equalToConstant: 2).isActive = true
    }
    
    fileprivate func setupHorizontalSliderWidthConstraint(with numberOfItem: Int) {
        // Width Anchor
        var multiplier: CGFloat = 1
        if numberOfItem != 0 {
            multiplier = CGFloat(1.0 / CGFloat(numberOfItem))
        }
        horizontalBarWidthANchorConstraint = swipingBarView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: multiplier)
        horizontalBarWidthANchorConstraint?.isActive = true
    }
    
    fileprivate func setupBottomDividerLine() {
        addSubview(bottomSeparator)
        bottomSeparator.backgroundColor = bottomDividerLineColor
        _ = bottomSeparator.anchor(top: bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
    }
    
    fileprivate func updateHorizontalBarConstraint(with numberOfItem: Int) {
        // Reset horizontal slider bar width constraint
        horizontalBarWidthANchorConstraint?.isActive = false
        horizontalBarWidthANchorConstraint = swipingBarView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: CGFloat(1.0 / CGFloat(numberOfItem)))
        horizontalBarWidthANchorConstraint?.isActive = true
    }
    
    fileprivate func updateCollectionView() {
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())
        delegate?.scrollToMenuIndex(menuIndex: 0)
        collectionView.reloadData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
