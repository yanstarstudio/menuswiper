//
//  MenuBar+Delegates.swift
//  SwippingMenuCollectionComponent
//
//  Created by Haiyan Ma on 05/04/2017.
//  Copyright © 2017 SwiftTsubame. All rights reserved.
//

import UIKit

// ----------------------------------------
// MARK: Collectionview Delegate
// ----------------------------------------
extension MenuBar: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.scrollToMenuIndex(menuIndex: indexPath.item)
    }
}

// ----------------------------------------
// MARK: Collectionview Flowlayout
// ----------------------------------------
extension MenuBar: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var widthMultiplier: CGFloat = 1
        
        if let number = self.numberOfItem, number != 0 {
            widthMultiplier = CGFloat(number)
        }
        return CGSize(width: self.frame.width / widthMultiplier, height: self.frame.height)
    }
}

// ----------------------------------------
// MARK: Collectionview Datasource
// ----------------------------------------
extension MenuBar: UICollectionViewDataSource {
    // MARK: collectionview datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfItem ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MenuBarCell

        cell.backgroundColor = barBackgroundColor
        
        cell.cellDatasource = CellDataSource(title:menuBarLabelTextArray?[indexPath.item],
                                             imageName: menuBarImageArray?[indexPath.item],
                                             alignment: menuAlignment)
        cell.titleStyle(colorOnSelection: menuTextSelectedColor, colorDefault: menuTextColor, font: titleFont)
        return cell
    }
}
