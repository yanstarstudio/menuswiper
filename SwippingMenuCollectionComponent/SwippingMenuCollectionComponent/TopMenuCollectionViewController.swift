//
//  ViewController.swift
//  SwippingMenuCollectionComponent
//
//  Created by Haiyan Ma on 05/04/2017.
//  Copyright © 2017 SwiftTsubame. All rights reserved.
//

import UIKit

protocol MenuScrollDelegate: class {
    
    func scrollToMenuIndex(menuIndex: Int)
}


open class TopMenuCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    open var menuBarHeight: CGFloat? = MenuBarStyle.defaultCellHeight {
        didSet {
            if let menuBarHeight = menuBarHeight {
                heightAnchor?.isActive = false
                heightAnchor?.constant = menuBarHeight
                heightAnchor?.isActive = true
                setupCollectionView()
                collectionView?.reloadData()
            }
        }
    }
    
    open var menuAlignment: UILayoutConstraintAxis? {
        didSet {
            menuBar.menuAlignment = menuAlignment
            let selectedIndexPath = IndexPath(item: 0, section: 0)
            menuBar.collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())
            collectionView?.reloadData()
        }
    }
    
    open let activityIndicatorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        aiv.hidesWhenStopped = true
        aiv.color = .black
        return aiv
    }()
    
    open func titleStyle(colorOnSelection: UIColor? = MenuBarStyle.onSelectionTitleColor,
                         colorDefault: UIColor? = MenuBarStyle.defaultTitleColor,
                         font: UIFont? = MenuBarStyle.titleFont) {

        if let colorOnSelection = colorOnSelection {
            menuBar.menuTextSelectedColor = colorOnSelection
            menuBar.sliderBackgroundColor = colorOnSelection
        }
        
        if let colorDefault = colorDefault {
            menuBar.menuTextColor = colorDefault
        }
        
        if let font = font {
            menuBar.titleFont = font
        }
        
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        menuBar.collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())
        collectionView?.reloadData()
    }
    
    open func menuStyle(backgroundColor: UIColor? = MenuBarStyle.menuBackgroundColor,
                        sliderColor: UIColor? = MenuBarStyle.onSelectionTitleColor,
                        bottomlineColor: UIColor? = MenuBarStyle.bottomLineColor) {
        
        if let backgroundColor = backgroundColor {
            menuBar.barBackgroundColor = backgroundColor
        }
        
        if let sliderColor = sliderColor {
            menuBar.sliderBackgroundColor = sliderColor
        }
        
        if let bottomlineColor = bottomlineColor {
            menuBar.bottomDividerLineColor = bottomlineColor
        }
        
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        menuBar.collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())
        collectionView?.reloadData()
    }
    
    var heightAnchor: NSLayoutConstraint?
    
    var menuDatasource: (text: [String]?, image: [String]?) {
        didSet {
            if let menuDatasource = menuDatasource.0 {
                menuBar.menuBarLabelTextArray = menuDatasource
            }
            
            if let menuImageSource = self.menuDatasource.1 {
                menuBar.menuBarImageArray = menuImageSource
            }
            
            let numberOfItems = menuItemNumber(menuDatasource: self.menuDatasource)
            menuBar.numberOfItem = numberOfItems
                        
            let selectedIndexPath = IndexPath(item: 0, section: 0)
            menuBar.collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())
            collectionView?.reloadData()
        }
    }
    
    fileprivate lazy var menuBar: MenuBar = {
        let mb = MenuBar()
        mb.delegate = self
        return mb
    }()
    
    // ----------------------------------------
    // MARK: View LifeCycle
    // ----------------------------------------
    override open func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        setupMenuBar()
        setupActivityIndicator()
        automaticallyAdjustsScrollViewInsets = false
    }
    
    fileprivate func setupMenuBar() {
        view.addSubview(menuBar)
        heightAnchor = menuBar.anchor(top: topLayoutGuide.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: menuBarHeight ?? 0).last
        heightAnchor?.isActive = true
    }
    
    // Horizontal scrolling collection view
    fileprivate func setupCollectionView() {
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }
        collectionView?.backgroundColor = .white
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.contentInset = UIEdgeInsets(top: menuBarHeight ?? 0, left: 0, bottom: 0, right: 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: menuBarHeight ?? 0, left: 0, bottom: 0, right: 0)
        collectionView?.isPagingEnabled = true // snap to cells while scrolling
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        menuBar.collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())

    }
    
    fileprivate func setupActivityIndicator() {
        view.addSubview(activityIndicatorView)
        activityIndicatorView.anchorCenterXToSuperview()
        activityIndicatorView.anchorCenterYToSuperview()
    }
    
    fileprivate func menuItemNumber(menuDatasource: (text: [String]?, image: [String]?)) -> Int {
        
        if menuDatasource.image == nil && menuDatasource.text == nil {
            return 0
        }
        
        if let menuText = menuDatasource.text {
            if let imageArray = menuDatasource.image {
                return min(menuText.count, imageArray.count)
                
            } else {
                return menuText.count
            }
        }  else {
            return menuDatasource.image?.count ?? 0
        }
    }
    
    open override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItemNumber(menuDatasource: self.menuDatasource)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height - (menuBarHeight ?? 0))
    }
}


// --------------------------------------------
// MARK:- Scroll Related Delegates
// --------------------------------------------
extension TopMenuCollectionViewController: MenuScrollDelegate {
    
    func scrollToMenuIndex(menuIndex: Int) {
        let indexPath = IndexPath(item: menuIndex, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition(), animated: true)
    }
    
    override open func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let divisor = menuItemNumber(menuDatasource: self.menuDatasource) == 0 ? 1 : menuItemNumber(menuDatasource: self.menuDatasource)
        menuBar.horizontalBarLeftAnchorConstraint?.constant = scrollView.contentOffset.x / CGFloat(divisor)
    }
    
    override open func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = targetContentOffset.pointee.x / view.frame.width
        let indexPath = IndexPath(item: Int(index), section: 0)
        menuBar.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition())
    }
}

protocol RequiredFunctions {
    func required()
}


