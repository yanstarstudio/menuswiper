//
//  AppDelegate.swift
//  SwippingMenuCollectionComponent
//
//  Created by Haiyan Ma on 05/04/2017.
//  Copyright © 2017 SwiftTsubame. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        let nav = UINavigationController(rootViewController: TestController(collectionViewLayout: UICollectionViewFlowLayout()))
        window?.rootViewController = nav
        return true
    }


}

